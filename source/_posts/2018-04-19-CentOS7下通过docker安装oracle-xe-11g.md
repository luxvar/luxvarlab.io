---
title: CentOS7下通过docker安装oracle-xe-11g
date: 2018-04-19 15:52:46
tags: 
- Docker
- Linux
- Oracle 
categories:
- Linux
---

 

### 1.	安装docker并启动docker服务
```bash
 sudo yum install docker
 sudo systemctl start docker
```
### 2. 下载oracle-xe-11g镜像
```bash
sudo docker pull sath89/oracle-xe-11g 
```


下载后执行 sudo docker ps –a 查看本地存在的所有镜像
```bash
 sudo docker ps -a
CONTAINER ID        IMAGE                  COMMAND             CREATED             STATUS              PORTS                                            NAMES
b460f21c1964        sath89/oracle-xe-11g   "/entrypoint.sh "   3 days ago          Up 12 minutes       0.0.0.0:1521->1521/tcp, 0.0.0.0:8080->8080/tcp   oracle-xe-11g
```

### 3.	运行oracle镜像为容器,并将oracle容器的8080 和1521端口映射到本机的8080 和1521端口.
```bash
 sudo docker run  -p 8080:8080 -p 1521:1521  --name oracle-xe-11g -d sath89/oracle-xe-11g
 ```


运行docker ps  查看所有正在运行的容器
```bash
sudo docker ps
CONTAINER ID        IMAGE                  COMMAND             CREATED             STATUS              PORTS                                            NAMES
b460f21c1964        sath89/oracle-xe-11g   "/entrypoint.sh "   3 days ago          Up 12 minutes       0.0.0.0:1521->1521/tcp, 0.0.0.0:8080->8080/tcp   oracle-xe-11g
```


### 4.启动oracle-xe-11g容器
```bash
sudo docker start oracle-xe-11g
oracle-xe-11g
```

至此配置完毕 ,浏览器访问localhost:8080/apex
 
### 5.登录oracle容器,然后执行sqlplus命令
账号system,密码oracle
```bash
sudo docker  exec -it oracle-xe-11g /bin/bash
```

```bash
root@b460f21c1964:/# sqlplus system/oracle

SQL*Plus: Release 11.2.0.2.0 Production on Thu Apr 19 07:58:10 2018

Copyright (c) 1982, 2011, Oracle.  All rights reserved.


Connected to:
Oracle Database 11g Express Edition Release 11.2.0.2.0 - 64bit Production

SQL> 
```



